export const postsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "CALL_API":
            return {
                ...state,
                loading: true,
            };
        case "SUCCESS":
            return {
                ...state,
                loading: false,
                posts: action.data,
            };
        case "ERROR":
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        case "ADD_POST":
            return {
                ...state, 
                posts: [action.data, ...state.posts]
            };
        case "EDIT_POST":
            return { ...state, 
                posts:state.posts.map( post =>
                    (post.id === action.data.id) ? 
                        action.data:post) 
            }
      default:
        return state;
    }
};

export const initialState = {
    posts: [],
    loading: false,
    error: null,
};