import React, { useEffect, useReducer } from "react";
import { Post } from "./Post";
import { CreatePost } from "./CreatePost";
import { postsReducer, initialState } from "./reducers/postReducer";
import axios from 'axios';

const PostItem = () => {
  const [state, dispatch] = useReducer(postsReducer, initialState);
  const { posts, loading, error } = state;
  useEffect(() => {
      dispatch({ type: "CALL_API" });
      const getPosts = async () => {
          let response = await axios.get('https://jsonplaceholder.typicode.com/posts');
          if (response.status == 200) {
              dispatch({ type: "SUCCESS", data: response.data });
              return;
          }
          dispatch({ type: "ERROR", error: response.error });
      };

      getPosts();
  }, []);

  const createNewPost = (e) => {
    e.preventDefault();
    const addPost = async () => {
      let response = await axios.post('https://jsonplaceholder.typicode.com/posts', {
        title: e.target.title.value,
        body: e.target.body.value
      })
      .then(function (response) {
        
      if (response.status == 201) {
          dispatch({ type: 'ADD_POST', data: response.data });
          return;
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  addPost();
  };

  const handleUpdateSubmit = (id, title, body) => {
    const editPost = async () => {
      let response = await axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        title: title,
        body: body
      })
      .then(function (response) {
       
        if (response.status == 200) {
            dispatch({ type: 'EDIT_POST', data: response.data });
            return;
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    editPost();
  };
 

  return (
      <div>
          {loading ? (
            <div className="container">
              <h6>loading posts...</h6>
            </div>
          ) : error ? (
              <p>{error}</p>
          ) : (
              <div className="container mt-5">
                  <CreatePost
                    handleSubmit={createNewPost}
                  />
                <h1 className="mt-5">Posts</h1>
                {posts.map(post => (
                  <Post
                    id={post.id}
                    key={post.id}
                    title={post.title}
                    body={post.body}
                    onUpdate={handleUpdateSubmit}
                  />
                ))}
            </div>
          )}
      </div>
  );
};

export default PostItem;
