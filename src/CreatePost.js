import React, { useEffect, useReducer } from "react";

export const CreatePost = ({handleSubmit}) => {
    return (
    <div>
    <h1>Add a post</h1>
    <form onSubmit={handleSubmit}>
      <label className="form-label h6">Title</label>
      <input placeholder="Title" name="title" className="form-control"/>
      <label className="form-label mt-3 h6">Body</label>
      <textarea placeholder="Body" name="body" className="form-control"/>
      <button 
        className="btn btn-primary mt-3"> Save
      </button>
    </form>
  </div>
    )
}

